import psycopg2
import control.Sql

conn = psycopg2.connect(host="srvalfameta", database="powerbi", user="postgres", password="alfa*184420")
cur = conn.cursor()


if __name__ == '__main__':
	try:
		control.Sql.sqlCliente(conn, cur)
		#Sql.sqlGateway(conn, cur)
		#Sql.sqlDataSource(conn, cur)
		control.Sql.sqlDataset(conn, cur)
		control.Sql.sqlAgendamentos(conn, cur)
		control.Sql.sqlRefresh(conn, cur)
	except Exception as e:
		print("Erro: " + str(e))
	finally:
		if conn is not None:
			cur.close()
			conn.close()