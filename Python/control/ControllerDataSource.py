import control.Auth
import model.DataSource
import control.ControllerDataset
import requests

datasources = []


def getDataSource(dataset):
    resposta = requests.get('https://api.powerbi.com/v1.0/myorg/admin/datasets/' + str(dataset) + '/datasources',
                            headers={'Content-Type': 'application/json',
                                     'Authorization': 'Bearer {}'.format(control.Auth.token)})

    resposta = resposta.json()
    try:
        if not resposta['value']:
            resposta = ''
        else:
            resposta = resposta['value']
    except:
        pass

    return resposta

def getDataSources():
    print("Obtendo DataSources")
    for dataset in control.ControllerDataset.datasets:
        datasetDatasources = []
        datasetDatasources.append(getDataSource(dataset.coddataset))
        for ds in datasetDatasources[0]:
            if isinstance(ds, dict):
                try:
                    datasources.append(model.DataSource.DataSource(ds['datasourceId'], ds['datasourceType'], dataset.coddataset, ds['gatewayId']))
                    #print(ds['datasourceId'] + ' ' + ds['datasourceType'] + ' ' + dataset.coddataset + ' ' + ds['gatewayId'])
                except Exception as e:
                    pass


