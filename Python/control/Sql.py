import control.ControllerDataset
import control.ControllerDataSource
import control.ControllerGateway
import control.ControllerRefresh
import control.ControllerAgendamento

def sqlCliente(conn, cur):
    control.ControllerDataset.getDatasets()
    for d in control.ControllerDataset.datasets:
        try:
            sql = "insert into public.cliente (email) values ('%s');" % (str(d.email))
            #print(sql)
            cur.execute(sql)
        except Exception as e :
            print("Erro no insert de cliente: " + str(e))
            pass
        finally:
            conn.commit()

def sqlGateway(conn, cur):
    control.ControllerGateway.getGateways()
    for g in control.ControllerGateway.gateways:
        try:
            sql = "insert into public.gateway (codgateway, nome) values ('%s', '%s');" % (str(g.codgateway), str(g.email))
            #print(sql)
            cur.execute(sql)
        except Exception as e :
            print("Erro no insert de gateway: " + str(e))
            pass
        finally:
            conn.commit()

def sqlDataSource(conn, cur):
    control.ControllerDataSource.getDataSources()
    for d in control.ControllerDataSource.datasources:
        try:
            sql = "INSERT INTO public.datasource (coddatasource, nome, codgateway) " \
                  "VALUES ('%s', '%s', '%s');" % (str(d.coddatasource), str(d.nome), str(d.codgateway))
            #print(sql)
            cur.execute(sql)
        except Exception as e :
            print("Erro no insert de gateway: " + str(e))
            pass
        finally:
            conn.commit()

def sqlDataset(conn, cur):
    for d in control.ControllerDataset.datasets:
        try:
            sql = "insert into public.dataset (coddataset, nome, email, url, ativo) " \
                  "values ('%s', '%s', '%s', '%s', 'S');" % (str(d.coddataset), str(d.nome), str(d.email),
                                                                    str(d.url))
            #print(sql)
            cur.execute(sql)
        except Exception as e:
            print("Erro no insert de dataset: " + str(e))
            pass
        finally:
            conn.commit()


def sqlAgendamentos(conn, cur):
    control.ControllerAgendamento.getAgendamentos()
    for a in control.ControllerAgendamento.agendamentos:
        try:
            if a.tipo == 'D':
                sql = "insert into public.agendamentodia (coddataset, dia, ativo) " \
                      "values ('%s','%s','%s');" % (str(a.coddataset), str(a.dia), str(a.ativo))
            elif a.tipo == 'H':
                sql = "insert into public.agendamentohora (coddataset, hora, ativo) " \
                      "values ('%s','%s','%s');" % (str(a.coddataset), str(a.hora), str(a.ativo))
            #print(sql)
            cur.execute(sql)

        except Exception as e:
            print("Erro no insert de refresh: " + str(e))
            pass
        finally:
            conn.commit()


def sqlRefresh(conn, cur):
    control.ControllerRefresh.getRefreshes()
    for r in control.ControllerRefresh.refreshs:
        try:
            sql = "insert into public.atualizacao (codatualizacao, coddataset, datainicio, datafim, tipo, status) " \
                  "values ('%s','%s','%s','%s','%s','%s');" % (str(r.codatualizacao), str(r.coddataset),
                                                                      str(r.datainicio), str(r.datafim), str(r.tipo),
                                                                      str(r.status))
            print(sql)
            cur.execute(sql)

        except Exception as e:
            print("Erro no insert de refresh: " + str(e))
            pass
        finally:
            conn.commit()
