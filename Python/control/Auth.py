import adal


senha20 = 'alfa*184420'
senha30 = 'alfa*184430'
client_id = '675f77ad-6c01-49a0-a87d-4fa88a5a5ae8'


def getToken(usuario):
    context = adal.AuthenticationContext('https://login.microsoftonline.com/alfameta.com.br', validate_authority=None,
                                         cache=None, api_version=None, timeout=None, enable_pii=False)
    token_json = None
    try:
        token_json = context.acquire_token_with_username_password(
            resource='https://analysis.windows.net/powerbi/api',
            username=usuario,
            password=senha20,
            client_id=client_id
        )
    except adal.AdalError:
        try:
            token_json = context.acquire_token_with_username_password(
                resource='https://analysis.windows.net/powerbi/api',
                username=usuario,
                password=senha30,
                client_id=client_id
            )
        except adal.AdalError as e:
            print("Erro no email " + str(usuario) + ': ' + str(e))
            pass
    # else:
    #     print("Erro ao tentar adquirir o token de autenticação.")


    return token_json


token = getToken('gabriel@alfameta.com.br')['accessToken']
