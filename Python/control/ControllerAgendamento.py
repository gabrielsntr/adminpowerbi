import control.Auth
import model.Agendamento
import requests

agendamentos = []


def getAgendamento(dataset):
    resposta = requests.get('https://api.powerbi.com/v1.0/myorg/admin/datasets/' + str(dataset) + '/refreshSchedule',
                            headers={'Content-Type': 'application/json',
                                     'Authorization': 'Bearer {}'.format(control.Auth.token)})

    resposta = resposta.json()
    try:
        if not resposta['value']:
            resposta = ''
        else:
            resposta = resposta['value']
    except:
        pass

    return resposta

def getAgendamentos():
    print("Obtendo Agendamentos")
    for dataset in control.ControllerDataset.datasets:

        datasetAgendamentos = []
        retAgendamento = getAgendamento(dataset.coddataset)

        if 'error' in retAgendamento:
            continue

        datasetAgendamentos.append(retAgendamento)

        for ag in datasetAgendamentos[0]['days']:
            if isinstance(ag, str):
                try:
                    agendamentos.append(model.Agendamento.Agendamento(dataset.coddataset, ag, None,
                                                                      datasetAgendamentos[0]['enabled'], 'D'))
                except Exception:
                    raise Exception

        for ag in datasetAgendamentos[0]['times']:
            if isinstance(ag, str):
                try:
                    agendamentos.append(model.Agendamento.Agendamento(dataset.coddataset, None, ag,
                                                                      datasetAgendamentos[0]['enabled'], 'H'))
                except Exception:
                    raise Exception