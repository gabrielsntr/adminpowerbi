import control.Auth
import model.Gateway
import control.ControllerDataset
import requests

gateways = []


def getGateway(usuario):
    token = control.Auth.getToken(usuario)
    resposta = ''
    if token:
        token = control.Auth.getToken(usuario)['accessToken']
        resposta = requests.get('https://api.powerbi.com/v1.0/myorg/gateways',
                            headers={'Content-Type': 'application/json',
                                     'Authorization': 'Bearer {}'.format(token)})

        resposta = resposta.json()
        try:
            if not resposta['value']:
                resposta = ''
            else:
                resposta = resposta['value']
        except:
            pass

    return resposta


def getGateways():
    print("Obtendo Gateways")
    for dataset in control.ControllerDataset.datasets:
        datasetGateways = []
        if dataset.email not in ['stefan@alfameta.com.br', 'denyo@alfameta.com.br', 'financeiro@alfameta.com.br', 'politica@alfameta.com.br']:
            datasetGateways.append(getGateway(dataset.email))
            for g in datasetGateways[0]:
                if isinstance(g, dict):
                    try:
                        gateways.append(model.Gateway.Gateway(g['id'], g['name']))
                    except:
                        pass
