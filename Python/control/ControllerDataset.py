import control.Auth
import model.Dataset
import requests


datasets = []


def getDataset():
    print("Obtendo DataSets")
    resposta = requests.get('https://api.powerbi.com/v1.0/myorg/admin/datasets',
                            headers={'Content-Type': 'application/json',
                                     'Authorization': 'Bearer {}'.format(control.Auth.token)})

    resposta = resposta.json()
    try:
        if not resposta['value']:
            resposta = ''
        else:
            resposta = resposta['value']
    except:
        pass

    return resposta

def getDatasets():
    for item in getDataset():
        datasets.append(model.Dataset.Dataset(item['id'], item['name'], item['configuredBy']))

