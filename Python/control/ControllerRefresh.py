import control.Auth
import control.ControllerDataset
import model.Refresh
import control.Utils
import requests

refreshs = []


def getRefresh(id):
    resposta = requests.get('https://api.powerbi.com/v1.0/myorg/admin/datasets/' + id + '/refreshes',
                            headers={'Content-Type': 'application/json',
                                     'Authorization': 'Bearer {}'.format(control.Auth.token)})

    resposta = resposta.json()
    try:
        if not resposta['value']:
            resposta = ''
        else:
            resposta = resposta['value']
    except:
        pass

    return resposta

def getRefreshes():
    print("Obtendo Atualizações")
    for dataset in control.ControllerDataset.datasets:
        datasetRefreshs = []
        datasetRefreshs.append(getRefresh(dataset.coddataset))
        print(dataset.email + " " + dataset.coddataset)
        for refresh in datasetRefreshs[0]:
            if isinstance(refresh, dict):
                try:
                    refreshs.append(model.Refresh.Refresh(refresh['id'], dataset.coddataset, control.Utils.fomataData(refresh['startTime']),
                                                    control.Utils.fomataData(refresh['endTime']), refresh['refreshType'], refresh['status']))
                except:
                    pass
